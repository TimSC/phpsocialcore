<?php

require_once('config.php');

$handle = new SQLite3(DB_PATH);

$sql = "CREATE TABLE IF NOT EXISTS sources (
    id   INTEGER PRIMARY KEY,
    type TEXT    NOT NULL,
    name TEXT    NOT NULL,
    url  TEXT    NOT NULL,
    key  TEXT,
    updated_at  INTEGER NOT NULL
);";

$handle->exec($sql);

$sql = "CREATE TABLE IF NOT EXISTS posts (
    id   INTEGER PRIMARY KEY,
    external_id   INTEGER NOT NULL,
    modified_at  INTEGER NOT NULL,
    title TEXT NOT NULL,
    content TEXT NOT NULL,
    excerpt TEXT NOT NULL,
    link TEXT NOT NULL,
	source_id INTEGER NOT NULL,
    FOREIGN KEY (source_id)
    REFERENCES sources(source_id) ON UPDATE CASCADE
                                  ON DELETE CASCADE
);";

$handle->exec($sql);

$sql = "CREATE UNIQUE INDEX idx_posts_source_external_id 
ON posts (source_id, external_id);";

$handle->exec($sql);

$sql = "CREATE INDEX idx_posts_modified_at 
ON posts (modified_at);";

$handle->exec($sql);

$sql = "CREATE INDEX idx_posts_source_modified_at 
ON posts (source_id, modified_at);";

$handle->exec($sql);

$res = $handle->query('SELECT COUNT(*) FROM sources');
$sourcesCount = 0;
while ($row = $res->fetchArray())
{
	$sourcesCount = $row[0];
}

if ($sourcesCount == 0)
{
	$sql = "INSERT INTO sources(name, url, updated_at, type) 
	   VALUES('Shades of Green', 'https://greenpompey.org.uk/shades-of-green/wp-json/wp/v2/posts', 0, 'wordpress')";

	$handle->exec($sql);

	$sql = "INSERT INTO sources(name, url, updated_at, type) 
	   VALUES('Let Pompey Breathe', 'https://greenpompey.org.uk/let-pompey-breathe/wp-json/wp/v2/posts', 0, 'wordpress')";

	$handle->exec($sql);

    //How to get account ID https://rknight.me/get-mastodon-account-id-from-username/
	$sql = "INSERT INTO sources(name, url, updated_at, type) 
	   VALUES('@timsc@home.social', 'https://home.social;109315003994417116', 0, 'mastodon')";

	$handle->exec($sql);
}
?>

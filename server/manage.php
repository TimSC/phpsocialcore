<?php
require_once('config.php');

session_start();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title>Manage Media Sources</title>
  </head>
  <body>

<?php


function UpdateSources(&$handle)
{

	$handle->exec('BEGIN EXCLUSIVE;');
	$inTransaction = true;

	$res = $handle->query('SELECT * FROM sources ORDER BY id ASC');

	while ($row = $res->fetchArray(SQLITE3_ASSOC))
	{
?>
		<div><?php echo $row['id']." ".$row['type']." ".$row['name']." ".$row['url']." ".$row['updated_at'];?>

		<form method="post" style="display: inline;"><input type="hidden" name="action" value="delete"/><input type="hidden" name="id" value="<?php echo $row['id']; ?>"/><input type="submit" value="Delete"/></form>

		</div>
<?php
	}

	if($inTransaction)
	{
		$handle->exec('COMMIT;');
		$inTransaction = false;
	}
}

if(!isset($_SESSION['auth']) or !$_SESSION['auth']) {

	if(isset($_POST['action']) and $_POST['action'] == "login" and $_POST['password'] == SITE_PASSWORD)
		$_SESSION['auth'] = 1;
}

if(!isset($_SESSION['auth']) or !$_SESSION['auth']) {
?>
	<form method="post">
		<input type="password" name="password"/>
		<input type="hidden" name="action" value="login"/><input type="submit"/>
	</form>
<?php

}
else {

$handle = new SQLite3(DB_PATH);

if(isset($_POST['action']) and $_POST['action'] == "add") {

	$stmt = $handle->prepare("INSERT INTO sources(type, name, url, key, updated_at)
		VALUES(:type, :name, :url, :key, :updated_at)");

	$stmt->bindValue(':type', $_POST['type'], SQLITE3_TEXT);
	$stmt->bindValue(':name', $_POST['name'], SQLITE3_TEXT);
	$stmt->bindValue(':url', $_POST['url'], SQLITE3_TEXT);
	$stmt->bindValue(':key', $_POST['key'], SQLITE3_TEXT);
	$stmt->bindValue(':updated_at', 0, SQLITE3_INTEGER);

	$result = $stmt->execute();
}

if(isset($_POST['action']) and $_POST['action'] == "delete") {

	$stmt = $handle->prepare("DELETE FROM sources WHERE id=:id");

	$stmt->bindValue(':id', $_POST['id'], SQLITE3_INTEGER);

	$result = $stmt->execute();
}


UpdateSources($handle);
?>
	<form method="post">
		type <input type="text" name="type"/><br/>
		name <input type="text" name="name"/><br/>
		url <input type="text" name="url"/><br/>
		key <input type="text" name="key"/><br/>
		<input type="hidden" name="action" value="add"/><input type="submit"/>
	</form>

<?php

}
?>  
</body>
</html>

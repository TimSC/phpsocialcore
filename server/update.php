<?php
require_once('config.php');
//require_once('twitter.php');

function UpsertPost($handle, $external_id, $timestamp, $title_rendered, 
	$content_rendered, $excerpt_rendered, $link, $source_id)
{

	//echo $id ." ". $title_rendered."\n";
	$upsertSupported = SQLite3::version()['versionNumber'] >= 3024000;

	if($upsertSupported)
	{
		$stmt = $handle->prepare("INSERT INTO posts(external_id, modified_at, title, content, excerpt, link, source_id)
   			VALUES(:external_id, :modified_at, :title, :content, :excerpt, :link, :source_id)
			ON CONFLICT(source_id, external_id) DO UPDATE
			SET modified_at=:modified_at, title=:title, content=:content, excerpt=:excerpt, link=:link
			WHERE source_id=:source_id AND external_id=:external_id;");
	}
	else
	{
		$stmt = $handle->prepare("UPDATE posts
			SET modified_at=:modified_at, title=:title, content=:content, excerpt=:excerpt, link=:link
			WHERE source_id=:source_id AND external_id=:external_id;");
	}

	$stmt->bindValue(':external_id', $external_id, SQLITE3_INTEGER);
	$stmt->bindValue(':modified_at', $timestamp, SQLITE3_INTEGER);
	$stmt->bindValue(':title', $title_rendered, SQLITE3_TEXT);
	$stmt->bindValue(':content', $content_rendered, SQLITE3_TEXT);
	$stmt->bindValue(':excerpt', $excerpt_rendered, SQLITE3_TEXT);
	$stmt->bindValue(':link', $link, SQLITE3_TEXT);
	$stmt->bindValue(':source_id', $source_id, SQLITE3_INTEGER);

	$result = $stmt->execute();

	if (!$upsertSupported and $handle->changes() == 0)
	{
		$stmt = $handle->prepare("INSERT INTO posts(external_id, modified_at, title, content, excerpt, link, source_id)
   			VALUES(:external_id, :modified_at, :title, :content, :excerpt, :link, :source_id)");

		$stmt->bindValue(':external_id', $external_id, SQLITE3_INTEGER);
		$stmt->bindValue(':modified_at', $timestamp, SQLITE3_INTEGER);
		$stmt->bindValue(':title', $title_rendered, SQLITE3_TEXT);
		$stmt->bindValue(':content', $content_rendered, SQLITE3_TEXT);
		$stmt->bindValue(':excerpt', $excerpt_rendered, SQLITE3_TEXT);
		$stmt->bindValue(':link', $link, SQLITE3_TEXT);
		$stmt->bindValue(':source_id', $source_id, SQLITE3_INTEGER);

		$result = $stmt->execute();
	}
}


function UpdateWordpressSource(&$handle, $row)
{
	//Get source update and merge into local database
	$context = stream_context_create(array(
		'http'=>array(
			'header'=>"User-Agent: ".USER_AGENT."\r\n"
		)));

	$json = file_get_contents($row['url'], false, $context);

	$data = json_decode($json);

	foreach ($data as $post)
	{
		$external_id = $post->{'id'};

		$content = $post->{'content'};
		$content_rendered = $content->{'rendered'};

		$excerpt = $post->{'excerpt'};
		$excerpt_rendered = $excerpt->{'rendered'};

		$title = $post->{'title'};
		$title_rendered = $title->{'rendered'};

		$modified_gmt = $post->{'modified_gmt'}."Z";
		$modified_gmt = DateTime::createFromFormat(DateTime::ISO8601, $modified_gmt);

		$link = $post->{'link'};

		UpsertPost($handle, $external_id, $modified_gmt->getTimestamp(), $title_rendered, 
			$content_rendered, $excerpt_rendered, $link, $row['id']);

	}
}

function UpdateTwitterSource(&$handle, $row)
{
	$cli = new TwitterClient($row['key']);

	$results = $cli->Search("from:".$row['url']);

	$data = $results->{'data'};
	foreach($data as $tweet)
	{
		//print_r($tweet);

		$external_id = $tweet->{'id'};
		// https://stackoverflow.com/a/48663004/4288232
		$timestamp = (new DateTime)->setTimestamp(strtotime($tweet->{'created_at'}));
		$title_rendered = "@".$row['url'];
		$content_rendered = '<p>'.$tweet->{'text'}.'</p>';
		$excerpt_rendered = '<p>'.$tweet->{'text'}.'</p>';
		$link = "https://twitter.com/i/web/status/".$tweet->{'id'};
		$source_id = $row['id'];

		UpsertPost($handle, $external_id, $timestamp->getTimestamp(), $title_rendered, 
			$content_rendered, $excerpt_rendered, $link, $source_id);
	}
}

function UpdateMastodonSource(&$handle, $row)
{

	$urlSplit = explode(";", $row['url']);
	$parsedUrl = parse_url($urlSplit[0]);
	$acctId = $urlSplit[1];

	$context = stream_context_create(array(
		'http'=>array(
			'header'=>"User-Agent: ".USER_AGENT."\r\n"
		)));

	$json = file_get_contents($parsedUrl['scheme']."://".$parsedUrl['host']."/api/v1/accounts/".$acctId."/statuses", false, $context);

	$toots = json_decode($json);

	foreach($toots as $toot)
	{
		//print_r($toot);

		$external_id = $toot->{'id'};
		// https://stackoverflow.com/a/48663004/4288232
		$timestamp = (new DateTime)->setTimestamp(strtotime($toot->{'created_at'}));

		if ($toot->{'reblog'} == null)
		{
			$title_rendered = '@'.$toot->{'account'}->{'acct'}.'@'.$parsedUrl['host'];
			$content_rendered = $toot->{'content'};
			$excerpt_rendered = $toot->{'content'};
			$link = $toot->{'url'};
		}
		else
		{
			$title_rendered = '@'.$toot->{'reblog'}->{'account'}->{'acct'};
			$content_rendered = $toot->{'reblog'}->{'content'};
			$excerpt_rendered = $toot->{'reblog'}->{'content'};
			$link = $toot->{'reblog'}->{'url'};
		}
		$source_id = $row['id'];

		UpsertPost($handle, $external_id, $timestamp->getTimestamp(), $title_rendered, 
			$content_rendered, $excerpt_rendered, $link, $source_id);
	}
}

function UpdateSources(&$handle)
{

	$handle->exec('BEGIN EXCLUSIVE;');
	$inTransaction = true;

	$res = $handle->query('SELECT * FROM sources ORDER BY updated_at ASC');

	while ($row = $res->fetchArray(SQLITE3_ASSOC))
	{
		//echo "{$row['id']} {$row['name']} {$row['updated_at']} \n";

		//Limit how often we update
		$now = new DateTime();
		$elapsed = $now->getTimestamp() - $row['updated_at'];
		if ($elapsed < UPDATE_PERIOD) 
		{
			//echo "Skipping as last updated was ".$elapsed." sec ago.\n";
			continue;
		}

		//Store when the update happened
		$stmt = $handle->prepare("UPDATE sources
			SET updated_at = :updated_at
			WHERE id=:id");
		$stmt->bindValue(':id', $row['id'], SQLITE3_INTEGER);
		$stmt->bindValue(':updated_at', $now->getTimestamp(), SQLITE3_INTEGER);

		$result = $stmt->execute();

		$handle->exec('COMMIT;');
		$inTransaction = false;

		if ($row['type'] == "wordpress")
			UpdateWordpressSource($handle, $row);
		else if ($row['type'] == "twitter")
			UpdateTwitterSource($handle, $row);
		else if ($row['type'] == "mastodon")
			UpdateMastodonSource($handle, $row);

		//Only update one source at a time
		break;
	}

	if($inTransaction)
	{
		$handle->exec('COMMIT;');
		$inTransaction = false;
	}
}

?>

<?php
require_once('config.php');
require_once('update.php');

$handle = new SQLite3(DB_PATH);

$res = $handle->query('SELECT * FROM sources');

$postsBySource = array();
$allPosts = array();

while ($row = $res->fetchArray(SQLITE3_ASSOC))
{
	$postRes = $handle->query('SELECT * FROM posts WHERE source_id = '.$row['id'].' ORDER BY modified_at DESC LIMIT 5');
	$postsData = array();

	while ($post = $postRes->fetchArray(SQLITE3_ASSOC))
	{
		//echo "{$post['id']} {$post['title']} {$post['modified_at']} \n";
		array_push($postsData, $post);
		array_push($allPosts, array($post['modified_at'], $post));
	}

	$postsBySource[$row['id']] = $postsData;
}

rsort($allPosts);

//Get the most fresh posts
$fresh = array_slice ($allPosts, 0, 5);

$freshIds = array();
foreach ($fresh as $postDt)
	$freshIds[$postDt[1]['id']] = true;

//Ensure at least 1 post from each source
$outPosts = $fresh;
foreach ($postsBySource as $source_id => $posts)
{
	//Count how many in fresh
	$count = 0;
	foreach ($posts as $post)
	{
		if(array_key_exists($post['id'], $freshIds))
			$count += 1;
	}

	if ($count==0 and count($posts) > 0)
	{
		//Add at least one post
		array_push($outPosts, array($posts[0]['modified_at'], $posts[0]));
	}
}

$outPostsVals = array();
foreach ($outPosts as $postDt)
{
	array_push($outPostsVals, $postDt[1]);
}

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");
$out = array("posts"=>$outPostsVals);
echo json_encode($out);

UpdateSources($handle);

?>
